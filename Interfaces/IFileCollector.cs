﻿using Factorin.Entity;

namespace Factorin.Interfaces;

public interface IFileCollector
{
    ResultFile Calculate(string path);
}