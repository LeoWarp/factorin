﻿using Factorin.Entity;
using Factorin.Interfaces;

namespace Factorin.FileTypes;

public class TxtFile : IFileCollector
{
    /// <summary>
    /// Взял отсюда: https://en.wikipedia.org/wiki/Whitespace_character
    /// </summary>
    private readonly string _punctuationMarks = ",.?!;:\\\"\\r\\n\\v\\f\\u0085\\u2028\\u2029";
    
    public ResultFile Calculate(string path)
    {
        try
        {
            var text = File.ReadAllText(path);
            if (string.IsNullOrWhiteSpace(text))
            {
                Console.WriteLine("В файле нет информации");
                return new ResultFile();
            }
            var punctuation = text.Count(x => _punctuationMarks.Contains(x));
            
            var fileName = Path.GetFileName(path);
            return new ResultFile()
            {
                FileName = fileName,
                OperationName = "Подсчёт количество знаков препинания",
                Result = $"Количество знаков препинания: {punctuation}"
            };
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new ResultFile();
        }
    }
}