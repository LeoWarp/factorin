﻿using Factorin.Entity;
using Factorin.Interfaces;

namespace Factorin.FileTypes;

public class CssFile : IFileCollector
{
    public ResultFile Calculate(string path)
    {
        try
        {
            var text = File.ReadAllText(path);
            if (string.IsNullOrWhiteSpace(text))
            {
                Console.WriteLine("В файле нет информации");
                return new ResultFile();
            }
            
            var countOfLeftBracket = text.Count(x => x == '{');
            var countOfRightBracket = text.Count(x => x == '}');
            
            var fileName = Path.GetFileName(path);
            return new ResultFile()
            {
                FileName = fileName,
                OperationName = "Подсчёт количество тегов div",
                Result = $"Количество открывающих скобок совпадает с количеством закрывающих скобок: {countOfLeftBracket == countOfRightBracket}"
            };
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new ResultFile();   
        }
    }
}