﻿using Factorin.Entity;
using Factorin.Interfaces;
using HtmlAgilityPack;

namespace Factorin.FileTypes;

public class HtmlFile : IFileCollector
{
    private readonly HtmlDocument _htmlDocument = new HtmlDocument();

    public ResultFile Calculate(string path)
    {
        try
        {
            var text = File.ReadAllText(path);
            if (string.IsNullOrWhiteSpace(text))
            {
                Console.WriteLine("В файле нет информации");
                return new ResultFile();
            }
            
            _htmlDocument.LoadHtml(text);
            var countOfTags = _htmlDocument.DocumentNode.SelectNodes("//div").Count();

            var fileName = Path.GetFileName(path);
            return new ResultFile()
            {
                FileName = fileName,
                OperationName = "Подсчёт количество тегов div",
                Result = $"Количество тегов: {countOfTags}"
            };
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new ResultFile();
        }
    }
}