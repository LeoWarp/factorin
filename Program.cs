﻿using Factorin.Entity;
using Factorin.Interfaces;
using static System.Console; 

WriteLine("Укажите путь к дериктории:");
var path = ReadLine();

if (string.IsNullOrWhiteSpace(path))
{
    throw new ArgumentNullException("Путь не указан");
}

using var watcher = new FileSystemWatcher(path);
watcher.EnableRaisingEvents = true;
watcher.Filter = "*.*";
watcher.Created += OnChanged;

void OnChanged(object source, FileSystemEventArgs e)
{
    var extension = Path.GetExtension(e.FullPath);

    FileFactory factory = new ConcreteFileFactory();
    IFileCollector fileCollector = factory.GetFileCollector(extension);
    var result = fileCollector.Calculate(e.FullPath);
    
    using (var sw = new StreamWriter($@"D:\Программирование\Проекты\Factorin\Factorin\report.txt", true))
    {
        sw.WriteLine($"{result.FileName}-{result.OperationName}-{result.Result}");
        sw.Close();
    }
}

ReadLine();