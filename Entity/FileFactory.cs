﻿using Factorin.Interfaces;

namespace Factorin.Entity;

public abstract class FileFactory
{
    public abstract IFileCollector GetFileCollector(string fileType);
}