﻿using Factorin.FileTypes;
using Factorin.Interfaces;

namespace Factorin.Entity;

public class ConcreteFileFactory : FileFactory
{
    public override IFileCollector GetFileCollector(string fileType)
    {
        switch (fileType)
        {
            case ".html":
                return new HtmlFile();
            case ".css":
                return new CssFile();
            default:
                return new TxtFile();
        }
    }
}