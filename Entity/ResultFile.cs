﻿namespace Factorin.Entity;

public class ResultFile
{
    public string FileName { get; set; }
    
    public string OperationName { get; set; }
    
    public string Result { get; set; }
}